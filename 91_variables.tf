#-----------------------------------------------------------
#
#-----------------------------------------------------------

variable "region" {
  default = "us-east-1"
}
variable "env" {
  default = "staging"
}
variable "tags" {
  default = {
    Owner   = "Ilka A."
    Project = "LLnix"
  }
}

# Network
#-----------------------------------------------------------
variable "vpc_cidr" {
  description = "VPC CIDR Block"
  default     = "10.0.0.0/16"
}
variable "public_subnet_cidrs" {
  default = [
    "10.0.1.0/24",
    "10.0.2.0/24",
  ]
}
variable "private_subnet_cidrs" {
  default = [
    "10.0.3.0/24",
    "10.0.4.0/24",
  ]
}

# Loadbalancer
#-----------------------------------------------------------
variable "health_check_path" {
  description = "Health check path for target group"
  default     = "/ping/"
}

# ECS
#-----------------------------------------------------------
variable "ecs_cluster_name" {
  default = "webinfra"
}
variable "instance_type" {
  default = "t2.micro"
}
variable "docker_image_django" {
  description = "Django docker image"
  #default = "<AWS_ACCOUNT_ID>.dkr.ecr.us-east-1.amazonaws.com/django-app:latest"
  default = "django-app:latest"
}
variable "docker_image_nginx" {
  description = "Nginx docker image"
  #default = "<AWS_ACCOUNT_ID>.dkr.ecr.us-east-1.amazonaws.com/django-app:latest"
  default = "nginx:latest"
}
variable "app_count" {
  description = "Number of docker containers to run"
  default     = 2
}
variable "django_allowed_hosts" {
  description = "Domain name for allowed hosts"
  default     = "*"
}

# Logs
#-----------------------------------------------------------
variable "log_retention_in_days" {
  default = "1"
}

# keypair
#-----------------------------------------------------------
variable "ssh_pubkey_file" {
  default = "~/.ssh/aws-keypair.pub"
}

# Autoscaling
#-----------------------------------------------------------
variable "autoscale_min" {
  default = "1"
}
variable "autoscale_max" {
  default = "3"
}
variable "autoscale_desired" {
  default = "2"
}

# Rds
#-----------------------------------------------------------
variable "rds_db_name" {
  default = "rdsdb"
}
variable "rds_username" {
  default = "administrator"
}
#variable "rds_password" {
#default = "NO DEFAULT"
#}
variable "rds_instance_class" {
  default = "db.t2.micro"
}

# Domain
#-----------------------------------------------------------
#variable "certificate_arn" {
#default = "NO DEFAULT"
#}
