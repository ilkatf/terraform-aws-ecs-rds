#-----------------------------------------------------------
# ./03_securitygroups.tf
#-----------------------------------------------------------

resource "aws_security_group" "load_balancer" {
  name        = "load-balancer-sg"
  description = "Web access to the ALB"
  vpc_id      = aws_vpc.main_vpc.id

  dynamic "ingress" {
    for_each = ["80", "443"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    description = "Allow ALL ports"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(var.tags, { Name = "${var.env}-load-balancer-sg" })
}

resource "aws_security_group" "ecs" {
  name        = "ecs-sg"
  description = "Acess from the ALB"
  vpc_id      = aws_vpc.main_vpc.id

  ingress {
    description     = "Allow ALL ports from ALB"
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    security_groups = [aws_security_group.load_balancer.id]
  }

  ingress {
    description = "Allow SSH access"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "Allow ALL ports"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(var.tags, { Name = "${var.env}-ecs-sg" })
}

resource "aws_security_group" "rds" {
  name        = "rds-sg"
  description = "Access from ECS"
  vpc_id      = aws_vpc.main_vpc.id

  ingress {
    description     = "Allow access from ECS"
    from_port       = "5432"
    to_port         = "5432"
    protocol        = "tcp"
    security_groups = [aws_security_group.ecs.id]
  }

  egress {
    description = "Allow ALL ports"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(var.tags, { Name = "${var.env}-rds-sg" })
}
