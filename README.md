# Terraform Aws ECS RDS

Terraform Aws ECS RDS

## Interactive visualization

*Blast Radius* is a tool for reasoning about Terraform dependency graphs with interactive visualizations.

To launch *Blast Radius* for current Terraform directory manually run:

```sh
docker run --rm -it -p 5000:5000 \
  -v $(pwd):/data:ro \
  --security-opt apparmor:unconfined \
  --cap-add=SYS_ADMIN \
  grubertech/blast-radius
```

## State list
To list all Terraform state
```sh
$ terraform state list
data.aws_ami.latest_amazon_ami_ecs
data.aws_availability_zones.available
data.aws_caller_identity.current
data.aws_ssm_parameter.rds_password
data.template_file.django_app
aws_alb_listener.ecs_alb_http_listener
aws_alb_target_group.default_target_group
aws_autoscaling_group.ecs_cluster
aws_cloudwatch_log_group.django_log_group
aws_cloudwatch_log_group.nginx_log_group
aws_cloudwatch_log_stream.django_log_stream
aws_cloudwatch_log_stream.nginx_log_stream
aws_db_instance.web_db
aws_db_subnet_group.web_sg
aws_ecs_cluster.ecs
aws_ecs_service.web_service
aws_ecs_task_definition.django_app
aws_eip.nat_eip[0]
aws_eip.nat_eip[1]
aws_iam_instance_profile.ecs
aws_iam_role.ecs_instance_role
aws_iam_role.ecs_service_role
aws_iam_role_policy.ecs_instance_role_policy
aws_iam_role_policy.ecs_service_role_policy
aws_internet_gateway.main_igw
aws_key_pair.ecs_keypair
aws_launch_configuration.ecs_lc
aws_lb.main_alb
aws_nat_gateway.nat_gw[0]
aws_nat_gateway.nat_gw[1]
aws_route_table.private_rtable[0]
aws_route_table.private_rtable[1]
aws_route_table.public_rtable
aws_route_table_association.private_association[0]
aws_route_table_association.private_association[1]
aws_route_table_association.public_association[0]
aws_route_table_association.public_association[1]
aws_security_group.ecs
aws_security_group.load_balancer
aws_security_group.rds
aws_ssm_parameter.rds_password
aws_subnet.private_subnets[0]
aws_subnet.private_subnets[1]
aws_subnet.public_subnets[0]
aws_subnet.public_subnets[1]
aws_vpc.main_vpc
random_password.rds_password
module.django_app.data.external.hash
module.django_app.aws_ecr_lifecycle_policy.repo-policy
module.django_app.aws_ecr_repository.repo
module.django_app.null_resource.push
module.nginx.data.external.hash
module.nginx.aws_ecr_lifecycle_policy.repo-policy
module.nginx.aws_ecr_repository.repo
module.nginx.null_resource.push
```
