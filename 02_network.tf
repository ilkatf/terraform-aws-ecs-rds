#-----------------------------------------------------------
# ./02_network.tf
#-----------------------------------------------------------

data "aws_availability_zones" "available" {}

#
#-----------------------------------------------------------

resource "aws_vpc" "main_vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = merge(var.tags, { Name = "${var.env}-vpc" })
}

resource "aws_internet_gateway" "main_igw" {
  vpc_id = aws_vpc.main_vpc.id

  tags = merge(var.tags, { Name = "${var.env}-igw" })
}

#
#-----------------------------------------------------------

resource "aws_subnet" "public_subnets" {
  count                   = length(var.public_subnet_cidrs)
  vpc_id                  = aws_vpc.main_vpc.id
  cidr_block              = element(var.public_subnet_cidrs, count.index)
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = true

  tags = merge(var.tags, { Name = "${var.env}-public-subnet-${count.index + 1}" })
}

resource "aws_route_table" "public_rtable" {
  vpc_id = aws_vpc.main_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main_igw.id
  }

  tags = merge(var.tags, { Name = "${var.env}-public-rtable" })
}

resource "aws_route_table_association" "public_association" {
  count          = length(aws_subnet.public_subnets[*].id)
  route_table_id = aws_route_table.public_rtable.id
  subnet_id      = aws_subnet.public_subnets[count.index].id
}

resource "aws_eip" "nat_eip" {
  count = length(var.private_subnet_cidrs)
  vpc   = true

  tags = merge(var.tags, { Name = "${var.env}-nat-eip-${count.index + 1}" })
}

resource "aws_nat_gateway" "nat_gw" {
  count         = length(var.private_subnet_cidrs)
  allocation_id = aws_eip.nat_eip[count.index].id
  subnet_id     = aws_subnet.public_subnets[count.index].id

  tags = merge(var.tags, { Name = "${var.env}-nat-gw-${count.index + 1}" })
}

#
#-----------------------------------------------------------

resource "aws_subnet" "private_subnets" {
  count             = length(var.private_subnet_cidrs)
  vpc_id            = aws_vpc.main_vpc.id
  cidr_block        = var.private_subnet_cidrs[count.index]
  availability_zone = data.aws_availability_zones.available.names[count.index]

  tags = merge(var.tags, { Name = "${var.env}-private-subnet-${count.index + 1}" })
}

resource "aws_route_table" "private_rtable" {
  count  = length(var.private_subnet_cidrs)
  vpc_id = aws_vpc.main_vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gw[count.index].id
  }

  tags = merge(var.tags, { Name = "${var.env}-private-rtable-${count.index + 1}" })
}

resource "aws_route_table_association" "private_association" {
  count          = length(aws_subnet.private_subnets[*].id)
  route_table_id = aws_route_table.private_rtable[count.index].id
  subnet_id      = aws_subnet.private_subnets[count.index].id
}
