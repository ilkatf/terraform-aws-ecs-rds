module "django_app" {
  source      = "./modules/terraform-aws-ecr-docker-image"
  image_name  = "django-app"
  source_path = "${path.root}/django-app"
}

module "nginx" {
  source      = "./modules/terraform-aws-ecr-docker-image"
  image_name  = "nginx"
  source_path = "${path.root}/nginx"
}
