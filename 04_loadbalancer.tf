#-----------------------------------------------------------
# ./04_loadbalancer.tf
#-----------------------------------------------------------

resource "aws_lb" "main_alb" {
  name               = "${var.ecs_cluster_name}-alb"
  load_balancer_type = "application"
  internal           = false
  security_groups    = [aws_security_group.load_balancer.id]
  subnets            = toset(aws_subnet.public_subnets[*].id)
  tags               = merge(var.tags, { Name = "${var.env}-${var.ecs_cluster_name}-alb" })
}

resource "aws_alb_target_group" "default_target_group" {
  name     = "${var.ecs_cluster_name}-target-group"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.main_vpc.id

  health_check {
    path                = var.health_check_path
    port                = "traffic-port"
    healthy_threshold   = 5
    unhealthy_threshold = 2
    timeout             = 2
    interval            = 5
    matcher             = "200"
  }
}

resource "aws_alb_listener" "ecs_alb_http_listener" {
  load_balancer_arn = aws_lb.main_alb.id
  port              = "80"   #"443"
  protocol          = "HTTP" #"HTTPS"
  #ssl_policy        = "ELBSecurityPolicy-2016-08"
  #certificate_arn   = var.certificate_arn
  depends_on = [aws_alb_target_group.default_target_group]

  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.default_target_group.arn
  }
}
