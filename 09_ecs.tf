#-----------------------------------------------------------
# ./08_ecs.tf
#-----------------------------------------------------------


data "aws_ami" "latest_amazon_ami_ecs" {
  owners      = ["591542846629"]
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-ecs-hvm-*-x86_64-ebs"]
  }
}

data "aws_caller_identity" "current" {}

locals {
  account_id              = data.aws_caller_identity.current.account_id
  docker_image_url_django = "${local.account_id}.dkr.ecr.${var.region}.amazonaws.com/${var.docker_image_django}"
  docker_image_url_nginx  = "${local.account_id}.dkr.ecr.${var.region}.amazonaws.com/${var.docker_image_nginx}"
}

resource "aws_ecs_cluster" "ecs" {
  name = "${var.ecs_cluster_name}-ecs-cluster"

  tags = merge(var.tags, { Name = "${var.env}-${var.ecs_cluster_name}-ecs-cluster" })
}

resource "aws_launch_configuration" "ecs_lc" {
  name                        = "${var.ecs_cluster_name}-ecs-cluster-lc"
  image_id                    = data.aws_ami.latest_amazon_ami_ecs.id #lookup(var.amis, var.region)
  instance_type               = var.instance_type
  security_groups             = [aws_security_group.ecs.id]
  iam_instance_profile        = aws_iam_instance_profile.ecs.name
  key_name                    = aws_key_pair.ecs_keypair.key_name
  associate_public_ip_address = true
  user_data                   = "#!/bin/bash\necho ECS_CLUSTER='${var.ecs_cluster_name}-ecs-cluster' > /etc/ecs/ecs.config"
}

data "template_file" "django_app" {
  template = file("templates/django_app.json.tpl")

  vars = {
    docker_image_url_django = local.docker_image_url_django
    docker_image_url_nginx  = local.docker_image_url_nginx
    region                  = var.region
    rds_db_name             = var.rds_db_name
    rds_username            = var.rds_username
    rds_password            = random_password.rds_password.result
    rds_hostname            = aws_db_instance.web_db.address
    allowed_hosts           = var.django_allowed_hosts
  }
}

resource "aws_ecs_task_definition" "django_app" {
  family                = "django-app"
  container_definitions = data.template_file.django_app.rendered
  depends_on            = [aws_db_instance.web_db, module.django_app.repository_url, module.nginx.repository_url]

  volume {
    name      = "static_volume"
    host_path = "/usr/src/app/staticfiles/"
  }
}

resource "aws_ecs_service" "web_service" {
  name            = "${var.ecs_cluster_name}-ecs-service"
  cluster         = aws_ecs_cluster.ecs.id
  task_definition = aws_ecs_task_definition.django_app.arn
  iam_role        = aws_iam_role.ecs_service_role.arn
  desired_count   = var.app_count

  depends_on = [aws_alb_listener.ecs_alb_http_listener, aws_iam_role_policy.ecs_service_role_policy]

  load_balancer {
    target_group_arn = aws_alb_target_group.default_target_group.arn
    container_name   = "nginx" #"django-app"
    container_port   = "80"
  }
}
