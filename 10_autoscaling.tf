resource "aws_autoscaling_group" "ecs_cluster" {
  name                 = "${var.ecs_cluster_name}-auto-scaling-group"
  min_size             = var.autoscale_min
  max_size             = var.autoscale_max
  desired_capacity     = var.autoscale_desired
  health_check_type    = "EC2"
  launch_configuration = aws_launch_configuration.ecs_lc.name
  vpc_zone_identifier  = toset(aws_subnet.private_subnets[*].id)
}
