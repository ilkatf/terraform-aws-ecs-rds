#-----------------------------------------------------------
# ./07_keypair.tf
#-----------------------------------------------------------

resource "aws_key_pair" "ecs_keypair" {
  key_name   = "${var.ecs_cluster_name}-key-pair"
  public_key = file(var.ssh_pubkey_file)

  tags = merge(var.tags, { Name = "${var.env}-ecs-key-pair" })
}
